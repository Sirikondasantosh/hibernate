package hibernate;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class ManageEmployee {

	private static SessionFactory factory;
	public static void main(String[] args) {
		
	factory=new AnnotationConfiguration().configure("hibernate.cfg.xml").buildSessionFactory();
	Session se=factory.openSession();
	Transaction txn= se.beginTransaction();
	Employee e1= new Employee();
	e1.setFirstname("sam");
	e1.setLastname("siri");
    e1.setSalary(25000);
    se.save(e1);
    txn.commit();
    
    se.close();
	
    System.out.println("done transaction ");
	
	}

}
